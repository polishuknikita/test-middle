import axios from 'axios'
import {urlApi} from './config'


export const planets = async (number) => axios.get(`${urlApi}/planets/${number}`).then(res => res.data)

