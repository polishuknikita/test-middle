import axios from 'axios'
import {urlApi} from './config'


export const people = async (number) => axios.get(`${urlApi}/people/${number}`).then(res => res.data)

