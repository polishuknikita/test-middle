import axios from 'axios'
import {urlApi} from './config'

const someUrl = `${urlApi}/people`

export const peopleList = async () => axios.get(someUrl).then(res => res.data)