import axios from 'axios'
import {urlApi} from './config'

const someUrl = `${urlApi}/planets`

export const planetsList = async () => axios.get(someUrl).then(res => res.data)