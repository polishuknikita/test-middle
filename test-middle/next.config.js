const path = require('path')

module.exports = {
  reactStrictMode: true,
  exportPathMap: function () {
    return {
      '/': { page: '/' },
    }
  },
  i18n: {
    locales: ['ru'],
    defaultLocale: 'ru',
  },

}
