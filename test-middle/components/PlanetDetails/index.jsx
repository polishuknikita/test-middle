import React from "react";

const PlanetDetails = (props) => {
  const planet = props.details;
  return (
    <div className="planet-details">
      <h3 className="planet-details__name">{planet.name}</h3>
      <span className="planet-details__climate">{planet.climate}</span>
      <span className="planet-details__diameter">{planet.diameter}</span>
      <span className="planet-details__gravity">{planet.gravity}</span>
      <span className="planet-details__orbital-period">
        {planet.orbital_period}
      </span>
      <span className="planet-details__population">{planet.population}</span>
      <span className="planet-details__terrain">{planet.terrain}</span>
    </div>
  );
};

export default PlanetDetails;
