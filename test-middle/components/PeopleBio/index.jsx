import React from "react";

const PeopleBio = (props) => {
  const peopleInfo = props.details;

  return (
    <section className="people-page">
      <div className="container">
        <div className="people-info">
          <h3 className="people-info__name">{`Name: ${peopleInfo.name}`}</h3>
          <span className="people-info__gender">
            {`Gender: ${peopleInfo.gender}`}
          </span>
          <span className="people-info__height">
            {`Height: ${peopleInfo.height}`}
          </span>
          {peopleInfo.birth_year !== "unknown" && (
            <span className="people-info__birth-year">
              {`Birth year: ${peopleInfo.birth_year}`}
            </span>
          )}
          <span className="people-info__eye-color">
            {`Eye color: ${peopleInfo.eye_color}`}
          </span>
          {peopleInfo.hair_color !== "none" && (
            <span className="people-info__hair-color">
              {`Hair color: ${peopleInfo.hair_color}`}
            </span>
          )}
          <span className="people-info__skin-color">
            {`Skin color: ${peopleInfo.skin_color}`}
          </span>
          {peopleInfo.mass !== "unknown" && (
            <span className="people-info__mass">{`Mass: ${peopleInfo.mass}`}</span>
          )}
        </div>
      </div>
    </section>
  );
};

export default PeopleBio;
