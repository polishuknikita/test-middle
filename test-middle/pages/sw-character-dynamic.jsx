import React from "react";
import { randomInteger } from "./index";
import PeopleBio from "../components/PeopleBio";
import { getPeople, getPeopleList } from "../../sw-character-static";

const swCharacterDynamic = (props) => {
  const peopleState = props.data;

  return peopleState && <PeopleBio details={peopleState} />;
};

export default swCharacterDynamic;

export async function getServerSideProps(context) {
  const queryId = context.query.id;
  const dataFullPeople = await getPeopleList();
  const randomId = await randomInteger(1, dataFullPeople.data.count);

  if (queryId) {
    const dataPeople = await getPeople(queryId);
    return {
      props: { data: dataPeople.data },
    };
  } else {
    const dataPeople = await getPeople(randomId);
    return {
      props: { data: dataPeople.data },
    };
  }
}
