import React from "react";
import Link from "next/link";
import PlanetDetails from "../../components/PlanetDetails";

const swPlanets = (props) => {
  return (
    <section className="planet-page">
      <div className="container">
        <PlanetDetails details={props.data} />

        <Link href={`/${props.randomPlanet.name}`}>
          <a className="planet-page-link">{`Next Random Planet: ${props.randomPlanet.name}`}</a>
        </Link>
      </div>
    </section>
  );
};

export const getPlanetsList = async () => {
  const resFullPlanets = await fetch(`https://swapi.dev/api/planets/`);
  const dataFullPlanets = await resFullPlanets.json(resFullPlanets);
  return {
    data: dataFullPlanets,
  };
};

export const getPlanets = async (queryId) => {
  const resPlanets = await fetch(`https://swapi.dev/api/planets/${queryId}`);
  const dataPlanets = await resPlanets.json(resPlanets);
  return {
    data: dataPlanets,
  };
};

export async function getServerSideProps(context) {
  const queryName = context.query.swPlanets;
  const dataFullPlanets = await getPlanetsList();

  const fullPlanets = [];

  for (let i = 1; i <= dataFullPlanets.data.count; i++) {
    let dataPlanet = await getPlanets(i);
    fullPlanets.push(dataPlanet.data);
  }

  const indexOfActivePlanet = fullPlanets.findIndex(
    (item) => item.name === queryName
  );

  const randomIndex = generateRandom(
    1,
    dataFullPlanets.data.count,
    indexOfActivePlanet + 1
  );

  const radomPlanet = await getPlanets(randomIndex);
  const dataPlanet = await getPlanets(indexOfActivePlanet + 1);
  return {
    props: { data: dataPlanet.data, randomPlanet: radomPlanet.data },
  };
}

export const generateRandom = (min, max, routIndex) => {
  let num = min + Math.random() * (max - min);
  return num === routIndex
    ? generateRandom(min, max, routIndex)
    : Math.round(num);
};

export default swPlanets;
