import React, { useEffect, useState } from "react";
import { peopleList } from "../outerApi/peopleList";
import { randomInteger } from "./index";
import { useRouter } from "next/router";
import { people } from "../outerApi/people";
import PeopleBio from "../components/PeopleBio";

const swCharacterClient = (props) => {
  const router = useRouter();

  const [peopleStateCount, setPeopleStateCount] = useState(0);
  const [peopleState, setPeopleState] = useState(!{});

  useEffect(() => {
    peopleList().then((res) => setPeopleStateCount(res.count));
  }, []);

  useEffect(() => {
    if (router.query.id) {
      people(router.query.id).then((res) => setPeopleState(res));
    }
  }, [router]);

  useEffect(async () => {
    if (peopleStateCount > 0) {
      const randomNumber = await randomInteger(1, peopleStateCount);
      people(randomNumber).then((res) => setPeopleState(res));
    }
  }, [peopleStateCount]);

  return peopleState && <PeopleBio details={peopleState} />;
};

export default swCharacterClient;
