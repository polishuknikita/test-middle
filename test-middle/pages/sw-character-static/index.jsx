import React, { useEffect } from "react";
import { randomInteger } from "../index";
import PeopleBio from "../../components/PeopleBio";
import { useRouter } from "next/router";

const swCharacterStatic = (props) => {
  const peopleState = props.people;
  const router = useRouter();

  useEffect(() => {
    if (router.query.id) {
      router.push(`/sw-character-static/${router.query.id}`, undefined, {
        shallow: true,
      });
    }
  }, [router]);
  return peopleState && <PeopleBio details={peopleState} />;
};

export const getStaticProps = async () => {
  const dataFullPeople = await getPeopleList();

  const randomId = await randomInteger(1, dataFullPeople.data.count);
  const randomPeople = await getPeople(randomId);
  return {
    props: {
      people: randomPeople.data,
    },
  };
};

export const getPeopleList = async () => {
  const resFullPeople = await fetch(`https://swapi.dev/api/people/`);
  const dataFullPeople = await resFullPeople.json(resFullPeople);
  return {
    data: dataFullPeople,
  };
};

export const getPeople = async (queryId) => {
  const resPeople = await fetch(`https://swapi.dev/api/people/${queryId}`);
  const dataPeople = await resPeople.json(resPeople);
  return {
    data: dataPeople,
  };
};

export default swCharacterStatic;
