import React from "react";
import PeopleBio from "../../../components/PeopleBio";
import { getPeopleList, getPeople } from "../index";

const swCharacterStaticId = (props) => {
  const peopleState = props.people.data;

  return peopleState && <PeopleBio details={peopleState} />;
};

export const getStaticPaths = async () => {
  const pageSlug = "id";

  const allPeopleList = await getPeopleList();
  const allPaths = [];

  for (let i = 1; i <= allPeopleList.data.count; i++) {
    allPaths.push({
      params: {
        [pageSlug]: i.toString(),
      },
    });
  }

  return {
    paths: allPaths,
    fallback: false,
  };
};

export const getStaticProps = async (context) => {
  const id = context.params.id;
  const data = await getPeople(id);
  return {
    props: {
      people: data,
    },
  };
};

export default swCharacterStaticId;
